# IronCrypt
IronCrypt is an easy-to-use application that encrypts and decrypts multiple files using the AES256-CBC algorithm. 

# Description
1. Open the application and enter the password of your choice (make it as big as possible)
2. Select the files you want to encrypt - decrypt
3. Press the Encryption - Decryption button correspondingly.
4. All Done

# License
The Project is under the MIT License.
