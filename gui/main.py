#!/usr/bin/env python

import os
from pathlib import Path

from tkinter import (
    Canvas,
    Entry,
    Button,
    PhotoImage,
    messagebox,
    StringVar, Toplevel, NW, Label,
)
from tkinter.messagebox import showinfo

from tkinter import filedialog as fd

import pyAesCrypt

OUTPUT_PATH = Path(__file__).parent
ASSETS_PATH = OUTPUT_PATH / Path("./assets")


def relative_to_assets(path: str) -> Path:
    return ASSETS_PATH / Path(path)


class IronCrypt(Toplevel):
    def __init__(self, *args, **kwargs):
        Toplevel.__init__(self, *args, **kwargs)
        self.protocol("WM_DELETE_WINDOW", self.on_closing)

        self.password = StringVar()
        self.filenames = list()

        self.geometry("700x500")
        self.resizable(False, False)
        self.configure(bg="#FFFFFF")

        self.canvas = Canvas(
            self,
            bg="#FFFFFF",
            height=432,
            width=797,
            bd=0,
            highlightthickness=0,
            relief="ridge",
        )

        self.canvas.place(x=0, y=0)

        enc_img = PhotoImage(file=relative_to_assets("enc_photo2.png"))

        self.canvas.create_image(
            380, 50, image=enc_img, anchor="nw")
        self.canvas.image = enc_img

        self.canvas.create_rectangle(
            40.0, 14.0, 742.0, 16.0, fill="#EFEFEF", outline=""
        )

        self.canvas.create_rectangle(
            40.0, 342.0, 742.0, 344.0, fill="#EFEFEF", outline=""
        )

        self.canvas.create_text(
            116.0,
            33.0,
            anchor="nw",
            text="IronCrypt Project",
            fill="#5E95FF",
            font=("Montserrat Bold", 26 * -1),
        )

        self.canvas.create_text(
            116.0,
            65.0,
            anchor="nw",
            text="Encrypt and Decrypt your files",
            fill="#808080",
            font=("Montserrat SemiBold", 16 * -1),
        )

        self.canvas.create_text(
            40.0,
            367.0,
            anchor="nw",
            text="Select Action:",
            fill="#5E95FF",
            font=("Montserrat Bold", 26 * -1),
        )

        self.canvas.create_text(
            96.0,
            250.0,
            anchor="nw",
            text="Choose password",
            fill="#808080",
            font=("Montserrat SemiBold", 16 * -1),
        )

        pwd_entry = Entry(
            self,
            bd=0,
            bg="#EFEFEF",
            highlightthickness=0,
            foreground="#777777",
            font=("Montserrat Bold", 18 * -1),
            textvariable=self.password,
        )

        pwd_entry.place(x=230.0, y=242.0, width=250.0, height=35.0)

        # Add sample data
        self.select_btn_img = PhotoImage(file=relative_to_assets("choose_btn.png"))
        self.select_files_btn = Button(
            self,
            image=self.select_btn_img,
            borderwidth=0,
            highlightthickness=0,
            command=self.handle_select_cb,
            relief="flat",
            state="normal",
        )

        self.select_files_btn.place(x=352.0, y=359.0, width=55.0, height=55.0)

        self.enc_btn_img = PhotoImage(file=relative_to_assets("enc_btn.png"))
        self.encrypt_btn = Button(
            self,
            image=self.enc_btn_img,
            borderwidth=0,
            highlightthickness=0,
            command=self.handle_encryption_cb,
            relief="flat",
            state="normal",
        )
        self.encrypt_btn.place(x=453.0, y=359.0, width=55.0, height=55.0)

        self.dec_btn_img = PhotoImage(file=relative_to_assets("dec_btn.png"))
        self.decrypt_btn = Button(
            self,
            image=self.dec_btn_img,
            borderwidth=0,
            highlightthickness=0,
            command=self.handle_decryption_cb,
            relief="flat",
            state="normal",
        )

        self.decrypt_btn.place(x=555.0, y=359.0, width=55.0, height=55.0)

    def on_closing(self):
        self.destroy()
        self.quit()
            
    def handle_encryption_cb(self):
        if self.password.get():
            for file in self.filenames:
                pyAesCrypt.encryptFile(file, file + '.enc', self.password.get())
                os.remove(file)
                os.rename(file + '.enc', file)

            messagebox.showinfo("Information", "Encryption Completed Successfully")

    def handle_decryption_cb(self):
        if self.password.get():
            for file in self.filenames:
                pyAesCrypt.decryptFile(file, file + '.dec', self.password.get())
                os.remove(file)
                os.rename(file + '.dec', file)

            messagebox.showinfo("Information", "Decryption Completed Successfully")

    def handle_select_cb(self):
        filetypes = (
            ('text files', '*.txt'),
            ('All files', '*.*')
        )

        filenames = fd.askopenfilenames(
            title='Open files',
            initialdir='/',
            filetypes=filetypes)

        showinfo(
            title='Selected Files',
            message=filenames
        )

        self.filenames = list(filenames)


