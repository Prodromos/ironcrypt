#!/usr/bin/env python

import tkinter as tk

# Main window constructor
from tkinter import messagebox

from gui.main import IronCrypt

root = tk.Tk()  # Make temporary window for app to start
root.withdraw()  # WithDraw the window


if __name__ == "__main__":

    root.title('IronCrypt')

    IronCrypt()
    root.mainloop()
